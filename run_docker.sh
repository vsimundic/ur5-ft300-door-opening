
docker rm ur5_ros
docker run --gpus all --runtime=runc --interactive -it \
--shm-size=10gb \
--env="DISPLAY" \
--volume="$(pwd):/home/ur5_user/ur5_ws" \
--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
--workdir="/home/ur5_user/ur5_ws" \
--privileged \
--name=ur5_ros \
ur5_ros:latest
# docker exec -it openpose2 bash
