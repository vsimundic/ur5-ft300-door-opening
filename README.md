# UR5OpenDoors

Implementartion of a door opening algorithm in force mode using UR5, FT300 and Robotiq 3F gripper. The algorithm for door opening uses Robot Operating System (ROS) for the gripper and the FT sensor control. It also uses RTDE library for communication with robot. 

# Table of Contents
- [Built with](#built-with)
- [Installation](#installation)
- [Usage](#usage)
- [Credits](#credits)
- [License](#license)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>


# Built with
This project was built with:
- Robot Operating System (ROS)
- [RTDE library](https://sdurobotics.gitlab.io/ur_rtde/)
- [fmauch/universal_robot](https://github.com/fmauch/universal_robot)
- [ UniversalRobots/Universal_Robots_ROS_Driver](https://github.com/UniversalRobots/Universal_Robots_ROS_Driver)

# Installation
Clone the repository:
```sh
git clone https://gitlab.com/vsimundic/ur5-ft300-door-opening ur5_ws
```
Enter the directory and build docker.
```sh
cd ur5_ws && ./build_docker.sh
```
Customize the run_docker.sh file and run it:
```sh
./run_docker.sh
```
When in docker container, run:
```sh
catkin_make
```

# Usage
The algorithm takes the following parameters as inputs:
- Starting pose of the robot's `tool0` joint in front of the door's handle
- The door width (in meters)
- Number of points for which the opening of the door will be carried out 
- Rotation between the above mentioned points (in degrees)
- IP address of the robot
- The robot's port (default is 30004)
- RTDE port (default is 30002)

All of the parameters can set in the config file located in `ft300s` package: [`ft300s/config/door_opening_config.yaml`](https://gitlab.com/vsimundic/ur5-ft300-door-opening/-/tree/master/src/ft300s/config/door_opening_config.yaml).

When the terminal is located in the `ur5_ws` workspace, source and run the necessary launch files:
```sh
source devel/setup.bash

roslaunch ur_robot_driver ur5_bringup.launch robot_ip:=<IP> kinematics_config:=/home/ur5_user/ur5_ws/src/Universal_Robots_ROS_Driver/UR5_calibration.yaml
```
```sh
source devel/setup.bash

roslaunch ft300s door_opening.launch
```

The robot then positions itself in front of the door, grabs the handle and performs a series of smaller movements computed by the algorithm to open the door. 

<!-- #TODO: images and videos -->

# Credits
This work has been fully supported by the Croatian Science Foundation under project
number IP-2019-04-6819.

# License

Distributed under the MIT License. See `LICENSE.txt` for more information.
