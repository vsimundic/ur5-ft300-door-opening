FROM nvidia/cuda:11.1.1-cudnn8-devel-ubuntu20.04

# RUN useradd -ms /bin/bash ur5_user

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y \
    build-essential libudev-dev usbutils libcanberra-gtk-module libcanberra-gtk3-module openjdk-8-jdk freeglut3-dev libusb-1.0-0-dev g++ curl \
    protobuf-compiler libprotoc-dev python3-opencv ca-certificates python3-dev git wget sudo  \
    cmake ninja-build 
ENV NO_AT_BRIDGE=1

# Install PIP
RUN wget https://bootstrap.pypa.io/pip/get-pip.py && \
    python3 get-pip.py && \
    rm get-pip.py

RUN ln -s /usr/bin/python3 /usr/bin/python

# PIP install
RUN pip install numpy matplotlib opencv-python scipy scikit-image tqdm open3d
RUN pip install ur-rtde

RUN apt-get install -y lsb-release
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
RUN apt install curl
RUN curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
RUN apt update
RUN apt install -y ros-noetic-desktop-full
RUN echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc
RUN apt install -y python3-rosdep python3-rosinstall python3-rosinstall-generator python3-wstool build-essential
RUN apt install -y python3-rosdep

RUN mkdir -p /home/ur5_user/ur5_ws/src
ADD src/ /home/ur5_user/ur5_ws/src/
RUN ls /opt/ros/noetic
RUN rosdep init
RUN rosdep update
WORKDIR /home/ur5_user/ur5_ws
RUN rosdep install --from-paths src --ignore-src -r --rosdistro noetic -y

RUN apt-get install -y ros-noetic-realsense2-camera
RUN apt-get install -y ros-noetic-openni-launch
RUN apt-get install -y ros-noetic-openni2-launch
RUN apt-get install -y ros-noetic-ros-numpy
RUN apt-get install -y ros-noetic-rosbash
RUN apt-get install -y ros-noetic-ros-control
RUN apt-get install -y ros-noetic-soem
RUN apt-get install -y ros-noetic-moveit
RUN apt-get install -y ros-noetic-trac-ik
RUN apt-get install -y python3-rospkg
RUN apt-get install -y software-properties-common
# RUN apt-get update
# RUN add-apt-repository ppa:sdurobotics/ur-rtde -y
# RUN apt-get update
# RUN apt install -y librtde librtde-dev

# USER ur5_user
# RUN /bin/bash -c '. /opt/ros/noetic/setup.bash; catkin_make'
# RUN catkin_make


# ENV PYTHONPATH="${PYTHONPATH}:/home/ur5_user/ur5_ws/"


WORKDIR /home/ur5_user/ur5_ws/src/ur_rtde
RUN mkdir build
WORKDIR /home/ur5_user/ur5_ws/src/ur_rtde/build
RUN cmake ..
RUN make
RUN make install

WORKDIR /home/ur5_user/ur5_ws